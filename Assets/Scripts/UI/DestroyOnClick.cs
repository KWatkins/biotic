using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnClick : MonoBehaviour
{
    private void OnEnable()
    {

    }
    public void DestroyPlayerAndCanvas()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        if (player != null)
        {
            Destroy(player);
        }
        if (canvas != null)
        {
            Destroy(canvas);
        }
    }

    public void DestorySceneManager()
    {
        GameObject SceneManager = GameObject.Find("SceneController");
        Destroy(SceneManager);
    }
}
