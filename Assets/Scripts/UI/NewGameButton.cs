using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewGameButton : MonoBehaviour
{
    Button button;
    [SerializeField]
    Signal transitionLevelSignal;
    private void OnEnable()
    {

        button = this.gameObject.GetComponent<Button>();
        SceneController SceneController = FindObjectOfType<SceneController>();
        button.onClick.AddListener(SceneController.TransitionLevel);
    }
}
