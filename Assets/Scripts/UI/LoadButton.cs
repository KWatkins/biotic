using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadButton : MonoBehaviour
{
    Button button;
    private void OnEnable()
    {
        button = this.gameObject.GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        SceneController SceneController = FindObjectOfType<SceneController>();
        button.onClick.AddListener(SceneController.LoadLevel);
    }
}
