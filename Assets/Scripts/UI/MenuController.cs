using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public void ButtonStart()
    {
        SceneManager.LoadScene(1);
        
    }    

    public void buttonQuit()
    {
        Application.Quit();
    }

    public void TestLevelButton()
    {
        SceneManager.LoadScene("TestArea");
    }

}
