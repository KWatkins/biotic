using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveButton : MonoBehaviour
{
    [SerializeField] Button button;

    private void OnEnable()
    {
        button = this.gameObject.GetComponent<Button>();
        PlayerController playerGo = FindObjectOfType<PlayerController>();
        Inventory playerInventory = FindObjectOfType<Inventory>();
        SceneController SceneController = FindObjectOfType<SceneController>();
        LevelManager currentLevelManager = FindObjectOfType<LevelManager>();
        button.onClick.AddListener(playerGo.SavePlayer);
        button.onClick.AddListener(currentLevelManager.SaveLevelItems);
        button.onClick.AddListener(SceneController.SaveLevel);

    }

}
