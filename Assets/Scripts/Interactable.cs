using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{

    [SerializeField] private GameObject objectToAnimate;
    private bool isInteracted;

    public bool hasAnimation;
    private Animator anim;

    [SerializeField] private bool ActivateOnInteract;
    [SerializeField] private GameObject objectToActivate;
 
    public bool alreadyActivated;

    [SerializeField] private bool spawnObjectOnInteract;
    [SerializeField] private float timeToSpawn;
    [SerializeField] private GameObject objectToSpawn;
    [SerializeField] private GameObject placeToSpawn;
    

    [SerializeField] AudioSource interactAudio;

    [SerializeField] private bool hasExtraSound = false;
    [SerializeField] AudioSource extraSound;

    public bool checkActivation = false;
    public bool loadActivation = false;
    void Start()
    {
        alreadyActivated = false;
        isInteracted = false;
        if(hasAnimation)
        {
        anim = objectToAnimate.GetComponent<Animator>();
        }
    }

    private void Update()
    {
        if(checkActivation == true && objectToActivate != null)
        {
            CheckActivation();
        }
        if(loadActivation == true)
        {
            LoadActivation();
        }
    }

    public void LoadActivation()
    {
        if(objectToActivate != null)
        {
            if (alreadyActivated)
            {
                anim.ResetTrigger("Close");
                anim.SetTrigger("Open");
            }
            else
            {
                anim.ResetTrigger("Open");
                anim.SetTrigger("Close");
            }
        }

    }
    public void CheckActivation()
    {
        if (anim != null)
        {
            if (!alreadyActivated)
            {
                anim.ResetTrigger("Close");
                anim.SetTrigger("Open");
                alreadyActivated = true;
            }
            else
            {
                anim.ResetTrigger("Open");
                anim.SetTrigger("Close");
                alreadyActivated = false;
            }
        }

        if (ActivateOnInteract && alreadyActivated == true)
        {
            objectToActivate.gameObject.SetActive(true);
            if (objectToActivate.gameObject.GetComponent<NPCMovementController>())
            {
                objectToActivate.gameObject.GetComponent<NPCMovementController>().isActivated = true;
            }
        }
        checkActivation = false;
    }

    public void PlayInteraction()
    {

        if (hasExtraSound)
        {
            extraSound.Pause();
        }


        objectToAnimate.GetComponent<AudioSource>().Play();

        if (objectToAnimate.GetComponent<AudioClip>() != null)
        {
            objectToAnimate.GetComponent<AudioSource>().Play();
        }

        if (spawnObjectOnInteract == true)
        {
            StartCoroutine(delaySpawn());
        }
    }

    IEnumerator delaySpawn()
    {
        yield return new WaitForSeconds(timeToSpawn);
        Instantiate(objectToSpawn, placeToSpawn.transform.position, Quaternion.Euler(0, 0, 90));
    }
}

