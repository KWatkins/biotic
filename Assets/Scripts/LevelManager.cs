using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{
    [SerializeField] public string currentLevel;

    [SerializeField]
    GameObject playerStart;

    [SerializeField]
    GameObject playerPrefab;
    [SerializeField]
    GameObject canvasPrefab;

    [SerializeField]
    InventoryData levelDefaultInventory;
    [SerializeField]
    InventoryData playersCurrentInventory;

    SceneController currentSceneController;

    public List<WorldItem> worldItems = new List<WorldItem>();
    public List<WorldItemData> worldItemData = new List<WorldItemData>();

    public LevelItemData levelItemDataScriptableObject;

    bool createCanvas = false;
    bool checkPlayersPosition = false;
    bool createPlayer = false;

    bool worldItemCheck = false;
    private void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(!player)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        //TestCreatePlayerAndCanvas();
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;

    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        CheckForWorldItems();
        currentSceneController = FindObjectOfType<SceneController>();
        if(currentSceneController != null)
        {
            currentSceneController.currentLevel = SceneManager.GetActiveScene().name;

            if (currentSceneController.loadlevel && !currentSceneController.transitionLevel) //Level Load
            {
                Debug.Log("LoadLevel");
                CreatePlayerAndCanvas();
                PlayerController currentPlayerController = FindObjectOfType<PlayerController>();
                currentPlayerController.LoadPlayer();
                PlayerManager.instance.player = currentPlayerController.gameObject;
                LoadLevelItems();
            }
            else if (currentSceneController.transitionLevel && !currentSceneController.loadlevel) //Level Transition
            {
                Debug.Log("LevelTransition");
                CreatePlayerAndCanvas();
            }
        }



    }

    private void SetPlayersPosition()
    {
        currentSceneController = FindObjectOfType<SceneController>();
        currentLevel = SceneManager.GetActiveScene().name;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (currentSceneController.transitionLevel)
        {
            CharacterController controller = player.gameObject.GetComponent<CharacterController>();
            if (player != null)
            {
                controller.enabled = false;
                controller.transform.position = playerStart.transform.position;
                controller.enabled = true;
                currentSceneController.transitionLevel = false;
            }
            else
            {
                controller.enabled = false;
                controller.transform.position = playerStart.transform.position;
                controller.enabled = true;
                currentSceneController.transitionLevel = false;
            }

        }
    }

    public void CreatePlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
        {

            Instantiate(playerPrefab, playerStart.transform.position, Quaternion.identity);
            PlayerController playerGo = FindObjectOfType<PlayerController>();
            if (currentSceneController.transitionLevel)
            {

            }
            else
            {
                playerGo.LoadPlayer();
            }
        }
    }

    public void SaveLevelItems()
    {
        worldItems.Clear();
        CheckForWorldItems();

        worldItemData.Clear();  //Clear the List so that we don't add duplicates.
        worldItemData = new List<WorldItemData>(); //Make a new list

        for (int i = 0; i < worldItems.Count; i++) //Loop through everything in world items
        {
            worldItemData.Add(new WorldItemData(worldItems[i].uniqueID, //create a new worldItemData with the variables and add it to the WorldItemData list.
                                                worldItems[i].objectID,
                                                worldItems[i].currentPosition,
                                                worldItems[i].currentRotation,
                                                worldItems[i].isNotActive));

        }
        SaveSystem.SaveLevelManager(this); //Another script copies the variables in here and serializes them. 
    }

    public void LoadLevelItems()
    {
        PlayerData data = SaveSystem.LoadLevelManager();  //This is where the saved data is unserialized
        List<WorldItemData> tempWorldItemData = new List<WorldItemData>(data.worldItemData);  //Make a temporary list so we don't overwrite our original list
        for (int i = tempWorldItemData.Count -1; i >= 0; i--)  //Go backwards through every item in the list incase we remove something
        {
            for (int j = worldItems.Count -1; j >= 0; j--)  //then go backwards through every item in the levels worldItems so we can look for duplicates
            {
                if (tempWorldItemData[i].uniqueID == worldItems[j].uniqueID)  //Checking for uniqueID matches
                {
                    Destroy(worldItems[j].gameObject); // get rid of the original object in the level
                    worldItems.RemoveAt(j);
                    GameObject newItem = Instantiate(levelItemDataScriptableObject.Prefab[tempWorldItemData[i].objectID], //create the same object where it was last saved
                                                                                          tempWorldItemData[i].currentPosition,
                                                                                          tempWorldItemData[i].currentRotation);
                    newItem.name = levelItemDataScriptableObject.Prefab[tempWorldItemData[i].objectID].name;
                    newItem.GetComponent<WorldItem>().uniqueID = tempWorldItemData[i].uniqueID;
                    if (tempWorldItemData[i].isNotActive == true)//check if object was inactive or not
                    {
                        newItem.SetActive(false);
                    }

                    tempWorldItemData.RemoveAt(i);
                    break;
                }
            }
        }
        for (int i = 0; i < tempWorldItemData.Count; i++)  //Go through the remaining items in the list that don't have a matching ID and create them. 
        {                                                 //This is everything that was created after the level started essentially. Enemy drops, new soda cans etc..
            GameObject newItem = Instantiate(levelItemDataScriptableObject.Prefab[tempWorldItemData[i].objectID], //Create item that was added after defeault scene start
                                                            tempWorldItemData[i].currentPosition,
                                                            tempWorldItemData[i].currentRotation);
            newItem.name = levelItemDataScriptableObject.Prefab[tempWorldItemData[i].objectID].name; //Give newitem the same name as the prefab it spawned
            if (tempWorldItemData[i].isNotActive == true) //check if object was inactive or not
            {
                newItem.SetActive(false);
            }
        }
    }

    public void CreatePlayerAndCanvas()
    {
        createPlayer = false;
        checkPlayersPosition = false;
        if (GameObject.FindGameObjectWithTag("Canvas") == null)
        {
            Instantiate(canvasPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        }
        if (!createPlayer)
        {
            CreatePlayer();
            createPlayer = true;
        }
        if (!checkPlayersPosition)
        {
            SetPlayersPosition();
            checkPlayersPosition = true;
        }

    }

    public void TestCreatePlayerAndCanvas()         //Testing purposes. Will delete soon.
    {

        Instantiate(canvasPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        CreatePlayer();
    }

    private void LoadDefaultInventory()
    {
        Inventory currentPlayerInventory = FindObjectOfType<Inventory>();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
        {
            Instantiate(playerPrefab, playerStart.transform.position, Quaternion.identity);
        }
        playersCurrentInventory.hasAxe = levelDefaultInventory.hasAxe;
        if (levelDefaultInventory.hasAxe)
        {

        }
        playersCurrentInventory.hasPistol = levelDefaultInventory.hasPistol;
        if (levelDefaultInventory.hasPistol)
        {
            currentPlayerInventory.ActivatePistol();
        }
        playersCurrentInventory.pistolCurrentAmmo.initialValue = levelDefaultInventory.pistolCurrentAmmo.initialValue;
        playersCurrentInventory.pistolCurrentCapacity.initialValue = levelDefaultInventory.pistolCurrentCapacity.initialValue;
        playersCurrentInventory.hasShotgun = levelDefaultInventory.hasShotgun;
        if (levelDefaultInventory.hasShotgun)
        {
            currentPlayerInventory.ActivateShotgun();
        }
        playersCurrentInventory.shotgunCurrentAmmo.initialValue = levelDefaultInventory.shotgunCurrentAmmo.initialValue;
        playersCurrentInventory.shotgunCurrentCapacity.initialValue = levelDefaultInventory.shotgunCurrentCapacity.initialValue;
        playersCurrentInventory.hasSMG = levelDefaultInventory.hasSMG;
        playersCurrentInventory.sMGCurrentAmmo.initialValue = levelDefaultInventory.sMGCurrentAmmo.initialValue;
        playersCurrentInventory.sMGCurrentCapacity.initialValue = levelDefaultInventory.sMGCurrentCapacity.initialValue;
        playersCurrentInventory.hasGrenade = levelDefaultInventory.hasGrenade;
        playersCurrentInventory.grenadeCurrentAmmo.initialValue = levelDefaultInventory.grenadeCurrentAmmo.initialValue;
    }          //To be fixed and implemented after save and load are fixed

    private void CheckForWorldItems()
    {
        WorldItem[] newWorldItems = FindObjectsOfType<WorldItem>();
        foreach (WorldItem t in newWorldItems)
        {
            if (t.uniqueID == "")
            {
                t.CreateUniqueID();
            }
            worldItems.Add(t);
        }
    }
}

