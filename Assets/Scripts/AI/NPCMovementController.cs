using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CrazyMinnow.SALSA;
public class NPCMovementController : MonoBehaviour
{

    public bool isMoving = false;
    private bool isIdle;

    [SerializeField] GameObject[] waypoints;
    public int currentWaypoint = 0;
    [SerializeField] float speed;
    float WPradius = 1;

    private AudioSource audioSource;

    Animator thisAnimator;

    public bool speakBeforeMove = false;

    [SerializeField] private AudioClip speakBeforeMoveClip;

    [SerializeField] private bool speakWhileMove = false;
    [SerializeField] private AudioClip[] speakWhileMovingClips;
    private AudioClip speakWhileMovingClip;
    private int clipNumber = 0;
    private bool alreadySpoke = false;

    [SerializeField] private GameObject player;
    private bool lookAtPlayer = false;

    public bool isActivated;
    public bool isAlive;

    private bool ActivateTalking = false;
    private bool ActivateWalking = false;
    private bool ActivateIdle = false;

    private float waitForAudio;
    private bool talkingCheck;

    private bool speakOnce = true;

    public Eyes randomEyes;
    public bool hasEyes;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        thisAnimator = this.gameObject.GetComponent<Animator>();
        if(speakWhileMove)
        {
            speakWhileMovingClip = speakWhileMovingClips[clipNumber];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("MainCamera");
        }

        if(hasEyes)
        {
            if (randomEyes == null)
            {
                randomEyes = FindObjectOfType<Eyes>();
                randomEyes.lookTarget = player.transform;
            }
        }

        if(hasEyes)
        {
            if (lookAtPlayer == true)
            {
                Vector3 targetPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                this.transform.LookAt(targetPosition);
            }
        }


        if (!audioSource.isPlaying && Time.timeScale != 0)
        {
            if (speakBeforeMove)
            {
                audioSource.clip = null;
                TalkingState();
            }

            if (isMoving)
            {
                if (ActivateWalking)
                {
                    WalkingState();
                }
            }


            if (!isMoving && !speakBeforeMove)
            { 
                if(ActivateIdle == true)
                {
                    IdleState();
                    ActivateIdle = false;
                }

            }
        }
        if (isActivated)
        {
            this.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.SetActive(false);
        }

        if (talkingCheck)
        {
            waitForAudio -= Time.deltaTime;
            if (waitForAudio <= 0)
            {
                ActivateIdle = false;
                ActivateTalking = false;
                speakBeforeMove = false;
                talkingCheck = false;
                isMoving = true;
                ActivateWalking = true;

            }
        }

        if (isMoving)
        {
            talkingCheck = false;
            if (currentWaypoint != waypoints.Length)
            {
                ActivateWalking = true;
                ActivateTalking = false;
                ActivateIdle = false;
                Vector3 relativePos = waypoints[currentWaypoint].transform.position - transform.position;
                transform.rotation = Quaternion.LookRotation(relativePos);

                transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWaypoint].transform.position, Time.deltaTime * speed);
                if (Vector3.Distance(waypoints[currentWaypoint].transform.position, transform.position) < WPradius)
                {
                    alreadySpoke = false;
                    if (currentWaypoint != waypoints.Length)
                    {
                        currentWaypoint = currentWaypoint + 1;
                    }
                }
            }
            if (currentWaypoint >= waypoints.Length)
            {
                thisAnimator.SetTrigger("isIdle");
                ActivateWalking = false;
                audioSource.Stop();
                speakWhileMove = false;
                ActivateIdle = true;
                isMoving = false;
            }

            if (speakWhileMove == true)
            {
                if (!audioSource.isPlaying && Time.timeScale != 0 && currentWaypoint < speakWhileMovingClips.Length && alreadySpoke == false)
                {
                    audioSource.clip = speakWhileMovingClips[currentWaypoint];
                    audioSource.Play();
                    alreadySpoke = true;
                }
            }
            else
            {
                alreadySpoke = true;
            }
        }

        if (speakBeforeMove)
        {
            ActivateTalking = true;
        }
        else
        {
            ActivateWalking = true;
        }
    }

    private void IdleState()
    {
        audioSource.Stop();
        thisAnimator.SetTrigger("isIdle");
        ActivateIdle = false;
        lookAtPlayer = true;
        if(speakOnce)
        {
            audioSource.clip = speakWhileMovingClips[currentWaypoint];
            audioSource.Play();
            speakOnce = false;
        }
        else
        {
            audioSource.clip = null;
        }

    }

    private void TalkingState()
    {
        audioSource.Stop();
        thisAnimator.SetTrigger("isTalking");
        audioSource.clip = speakBeforeMoveClip;
        audioSource.Play();
        waitForAudio = speakBeforeMoveClip.length;
        talkingCheck = true;
        ActivateTalking = false;


    }

    private void WalkingState()
    {
        audioSource.Stop();
        thisAnimator.SetTrigger("isWalking");
        audioSource.clip = speakWhileMovingClips[currentWaypoint];
        audioSource.Play();
        ActivateWalking = false;

        if(speakBeforeMove == false && currentWaypoint == waypoints.Length)
        {
            IdleState();
        }
    }

    private void PanickedState()
    {

    }

    public void StateCheck()
    {
        if(speakBeforeMove)
        {
            if (thisAnimator != null)
            {
                thisAnimator.SetTrigger("isTalking");
            }
        }

        if(isMoving)
        {
            if (thisAnimator != null)
            {
                thisAnimator.SetTrigger("isWalking");
            }
        }

        if (!isMoving)
        {
            if (thisAnimator != null)
            {
                thisAnimator.SetTrigger("isIdle");
            }
        }
    }

    public void JumpEvent()
    {
        StartCoroutine(Lerp(transform.position, transform.position + new Vector3(0,60f,0), speed));
    }

    private IEnumerator Lerp(Vector3 positionStart, Vector3 positionFinish, float time)
    {
        float elapsedTime = 0;

        while (elapsedTime < time)
        {
            transform.position = Vector3.Lerp(positionStart, positionFinish, elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
