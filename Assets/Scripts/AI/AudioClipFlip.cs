using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipFlip : MonoBehaviour
{
    [SerializeField]
    AudioClip[] amountOfClips;

    int audioClipPlaying = 0;

    AudioSource currentAudioSource;

    public void PlayNextAudioClip()
    {
        if(!currentAudioSource)
        {
            currentAudioSource = this.gameObject.GetComponent<AudioSource>();
        }
        if(amountOfClips != null)
        {
            currentAudioSource.clip = amountOfClips[audioClipPlaying];
            currentAudioSource.Play();
            audioClipPlaying++;
        }
    }
}
