using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour
{
    [SerializeField] public bool isPatroling;
    [SerializeField] public bool isWorking;


    [SerializeField] GameObject[] waypoints;
    int current = 0;
    [SerializeField] float speed;
    float WPradius = 1;
    Animator thisAnimator;

    private void OnEnable()
    {
        thisAnimator = this.gameObject.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (isPatroling)
        {
            if (Vector3.Distance(waypoints[current].transform.position, transform.position) < WPradius)
            {
                current = Random.Range(0, waypoints.Length);
                if (current >= waypoints.Length)
                {
                    current = 0;
                }
            }
            transform.position = Vector3.MoveTowards(transform.position, waypoints[current].transform.position, Time.deltaTime * speed);
        }

        if (isWorking)
        {
            if (Vector3.Distance(waypoints[current].transform.position, transform.position) < WPradius)
            {
                    thisAnimator.SetBool("isWorking", true);
            }
            transform.position = Vector3.MoveTowards(transform.position, waypoints[current].transform.position, Time.deltaTime * speed);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            other.gameObject.transform.parent = gameObject.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = null;
        }
    }
}
