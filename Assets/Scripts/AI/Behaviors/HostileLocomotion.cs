using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileLocomotion : EnemyBehaviorBase
{

    public float attackRange;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        thisAgent.speed = 7f;
        float dist = Vector3.Distance(opponent.transform.position, thisAgent.transform.position);

        if (dist < attackRange)
        {
            animator.SetTrigger("Attacking");
        }
        else
        {
            thisAgent.isStopped = false;
        }


    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        opponent = GameObject.FindGameObjectWithTag("Player");
        float dist = Vector3.Distance(opponent.transform.position, thisAgent.transform.position);
        if (dist < attackRange)
        {
            animator.SetTrigger("Attacking");
        }
        else
        {
        thisAgent.destination = opponent.transform.position;
        }

        float speed = thisAgent.velocity.magnitude;
        animator.SetFloat("MoveX", speed);




    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        thisAgent.isStopped = true;
    }
}
