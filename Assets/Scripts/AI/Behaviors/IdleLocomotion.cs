using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class IdleLocomotion : EnemyBehaviorBase
{
    public float walkRadius;
    public float speed;
    public float turnSpeed;

    public bool reachedDestination = true;
    Vector3 destination;

    public bool facingDestination;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        thisAgent.speed = .6f;
        destination = RandomNavmeshLocation(12f);


        if(facingDestination)
        {
            thisAgent.SetDestination(destination);
        }

        if (thisAgent.remainingDistance <= 1f)
        {
            reachedDestination = true;
        }
        else
        {
            reachedDestination = false;
        }

        if (reachedDestination)
        {
            destination = RandomNavmeshLocation(12f);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float dist = Vector3.Distance(opponent.transform.position, thisAgent.transform.position);

        if (dist < SightRadius)
        {
            animator.SetBool("isIdle", false);
            animator.SetBool("isHostile", true);
        }
        else
        {
            animator.SetBool("isIdle", true);
            animator.SetBool("isHostile", false);
        }
        if (!facingDestination)
        {
            // Determine which direction to rotate towards
            Vector3 targetDirection = destination - thisAgent.transform.position;

            // The step size is equal to speed times frame time.
            float singleStep = turnSpeed * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(thisAgent.transform.forward, targetDirection, singleStep, 0.0f);

            // Calculate a rotation a step closer to the target and applies rotation to this object
            thisAgent.transform.rotation = Quaternion.LookRotation(newDirection);

            if (Quaternion.Angle(thisAgent.transform.rotation, Quaternion.LookRotation(newDirection)) <= 0.01f)
            {
                facingDestination = true;
            }
        }

        float turningDir = AngleDir(thisAgent.transform.forward, destination, Vector3.up);
        float speed = thisAgent.velocity.magnitude;
        animator.SetFloat("MoveX", speed * 2f);
        animator.SetFloat("Turn", turningDir);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += NPC.transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        destination = finalPosition;
        return finalPosition;
    }

    void RotateTowardsDestination(Vector3 Destination)
    {
        Vector3 relativePos = Destination - thisAgent.transform.position;

        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        thisAgent.transform.rotation = rotation;
    }

    public void idleStance(int seconds)
    {

    }


    //returns -1 when to the left, 1 to the right, and 0 for forward/backward
    public float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0.0f)
        {
            return 1.0f;
        }
        else if (dir < 0.0f)
        {
            return -1.0f;
        }
        else
        {
            return 0.0f;
        }
    }
}
