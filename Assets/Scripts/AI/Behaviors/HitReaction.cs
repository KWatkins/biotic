using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitReaction : EnemyBehaviorBase
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
         base.OnStateEnter(animator, stateInfo, layerIndex);

    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        thisAgent.isStopped = true;
        thisAgent.ResetPath();
        thisAgent.velocity = Vector3.zero;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (thisAgent.isStopped)
        {
           //thisAgent.isStopped = false;
        }
    }
}
