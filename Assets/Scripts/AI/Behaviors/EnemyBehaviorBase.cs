using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviorBase : StateMachineBehaviour
{

    public GameObject NPC;
    public GameObject opponent;
    public float SightRadius;
    bool idle;
    bool hostile;
    bool attacking;
    bool staggered;


    Animator thisAnimator;
    public NavMeshAgent thisAgent;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NPC = animator.gameObject;
        if(NPC != null)
        {
            opponent = NPC.GetComponentInParent<EnemyAI>().FindPlayer();
        }

        if (thisAgent == null)
        {
            thisAgent = NPC.GetComponentInParent<NavMeshAgent>();
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(opponent == null)
        {
            opponent = NPC.GetComponent<EnemyAI>().FindPlayer();
        }

        if(NPC == null)
        {
            GameObject animatorGO = animator.gameObject;
            NPC = animatorGO.transform.parent.gameObject;
            if(thisAnimator == null)
            {
                thisAnimator = animator;
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
