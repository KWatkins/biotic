using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieSimple : MonoBehaviour
{

    [SerializeField] Transform agentDestination;

    private NavMeshAgent zombieAgent;
    private bool isDead;

    Health thisHealth;
    // Start is called before the first frame update
    void Start()
    {
        zombieAgent = this.GetComponent<NavMeshAgent>();
        thisHealth = this.GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        isDead = thisHealth.isDead;
        if (!isDead)
        {
            if (zombieAgent == null)
            {
                Debug.LogError("No Nav Mesh Agent attached to " + gameObject.name);
            }
            else
            {
                SetDestination();
            }
        }
        else
        {
            zombieAgent.isStopped = true;
        }

        if(isDead)
        {
            AudioSource zombie = this.gameObject.GetComponent<AudioSource>();
            zombie.Stop();
        }

    }

    private void SetDestination()
    {
        if (agentDestination != null)
        {
            Vector3 targetVector = agentDestination.transform.position;
            zombieAgent.SetDestination(targetVector);
        }
    }




}
