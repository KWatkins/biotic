using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : PickupItem
{
    [SerializeField]
    IntValue ammoToIncrease;
    [SerializeField]
    IntValue ammoToIncreaseMax;
    [SerializeField]
    int amountToIncrease;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            if (ammoToIncrease.initialValue != ammoToIncreaseMax.initialValue)
            {
               ammoToIncrease.initialValue += amountToIncrease;
               isPickedUp = true;
               this.gameObject.SetActive(false);
            }
        }
    }
}
