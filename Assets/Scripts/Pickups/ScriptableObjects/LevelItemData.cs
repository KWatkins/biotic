using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelItemData : ScriptableObject
{
    [SerializeField] public GameObject[] Prefab;
}
