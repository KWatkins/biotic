using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldItem: MonoBehaviour
{
    public string uniqueID;
    public int objectID = 0;
    public SerializableVector3 currentPosition;
    public SerializableVector3 lastKnownPosition;
    public SerializableQuaternion currentRotation;
    public SerializableQuaternion lastKnownRotation;
    public bool isNotActive;

    private Vector3 startingPosition;
    private void Awake()
    {
        if (uniqueID == "")
        {
            startingPosition = this.transform.position;
            CreateUniqueID();
        }

        if (isNotActive)
        {
            this.gameObject.SetActive(false);
        }

    }

    private void Update()
    {
        if (lastKnownPosition != this.transform.position)
        {
            lastKnownPosition = this.transform.position;
            currentPosition = lastKnownPosition;
        }

        if(lastKnownRotation != this.transform.rotation)
        {
            lastKnownRotation = this.transform.rotation;
            currentRotation = lastKnownRotation;
        }
    }

    public void CreateUniqueID()
    {
        if (uniqueID == "")
        {
            Vector3 tempPosition;
            tempPosition = startingPosition;
            string tempPositionString = tempPosition.ToString();
            string tempName = this.name.Substring(0, 2);
            uniqueID = tempName + tempPositionString;
        }
    }
}
