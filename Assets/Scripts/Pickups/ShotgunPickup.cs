using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunPickup : PickupItem
{
    public Signal pickupSignal;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !other.isTrigger)
        {
            pickupSignal.Raise();
            if (!playerCurrentInventory.hasShotgun)
            {
                playerCurrentInventory.hasShotgun = true;
            }
            playerCurrentInventory.shotgunCurrentCapacity.initialValue += 6;
            isPickedUp = true;
            this.gameObject.SetActive(false);
        }
    }
}
