using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : WorldItem
{
    public bool isPickedUp;
    public InventoryData playerCurrentInventory;

    private void Update()
    {
        if(isPickedUp)
        {
            Destroy(this.gameObject);
        }

        if (lastKnownPosition != this.transform.position)
        {
            lastKnownPosition = this.transform.position;
            currentPosition = lastKnownPosition;
        }
    }
}
