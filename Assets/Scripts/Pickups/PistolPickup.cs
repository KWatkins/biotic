using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolPickup : PickupItem
{
    public Signal pickupSignal;
    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && !other.isTrigger)
        {
            pickupSignal.Raise();
            if(!playerCurrentInventory.hasPistol)
            {
                playerCurrentInventory.hasPistol = true;
            }
            playerCurrentInventory.pistolCurrentCapacity.initialValue += 26;
            isPickedUp = true;
            this.gameObject.SetActive(false);
        }
    }
}
