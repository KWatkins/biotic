using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class PlayerController : MonoBehaviour
{

    private CharacterController controller;
    private Vector3 velocity;


    private float speed;
    [SerializeField] private float regularSpeed = 8f;
    [SerializeField] private float runSpeed = 10f;
    private float crouchSpeed = 4f;
    public float gravity = -9.8f;
    [SerializeField] private float jumpHeight = 2f;

    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    [SerializeField] LayerMask groundMask;

    [SerializeField] private Transform ceilingCheck;
    private bool ceilingHit;

    private bool isGrounded;

    public bool toggleCrouch = false;
    private float playerOriginalHeight;
    private float playerOriginalSpeed;

    private bool toggleMovement = true;

    [SerializeField] private float CrouchHeight = 0.5f;

    private AudioSource audioSource;

    public IntValue currentHealth;
    private Health Playerhealth;
    public float currentPlayerHealth;
    public bool isDead = false;

    [SerializeField] CinemachineVirtualCamera playerVCam;
    [SerializeField] float cameraYCrouchOffset;
    private float cameraYOriginal;
    private CinemachineTransposer playerTransposer;

    [SerializeField] private Vector3 playerVelocity;

    [SerializeField]
    public InventoryData playersCurrentInventory;

    public SaveInventoryItem playerSavedInventory;

    SceneController currentSceneController;

    void Start()
    {
        controller = this.gameObject.GetComponent<CharacterController>();
        playerVelocity = this.gameObject.GetComponent<CharacterController>().velocity;
        playerTransposer = playerVCam.GetCinemachineComponent<CinemachineTransposer>();
        cameraYOriginal = playerTransposer.m_FollowOffset.y;
        Application.targetFrameRate = 60;
        playerOriginalHeight = controller.height;
        playerOriginalSpeed = 8f;
        audioSource = GetComponent<AudioSource>();
        crouchSpeed = 4f;
        Playerhealth = this.gameObject.GetComponent<Health>();
        speed = regularSpeed;
        currentSceneController = FindObjectOfType<SceneController>();
        if (currentSceneController.loadlevel)
        {
            LoadPlayer();
        }

    }
    void Update()
    {
        isDead = Playerhealth.isDead;
        currentPlayerHealth = Playerhealth.health;

        if (!isDead)
        {
            Crouch();
            Jump();
            Movement();
            Run();
        }
        else
        {
            // you are dead
        }

        velocity.y += gravity * Time.deltaTime;
    }

    void FixedUpdate()
    {
        GroundCheck();
        CeilingCheck();
    }
    void Movement()
    {
        if (toggleMovement)
        {

            float x = Input.GetAxisRaw("Horizontal");
            float z = Input.GetAxisRaw("Vertical");


            Vector3 move = transform.right * x * speed
               + transform.forward * z * speed
               + transform.up * velocity.y;


            move = move.normalized * speed;
            controller.Move(move * Time.deltaTime);

            if (isGrounded == true && GetComponent<AudioSource>().isPlaying == false)
            {
                if (x != 0 || z != 0)
                {
                    audioSource.volume = Random.Range(0.8f, 1f);
                    audioSource.pitch = Random.Range(0.8f, 1.1f);
                    GetComponent<AudioSource>().Play();
                }
                else
                {
                    GetComponent<AudioSource>().Stop();
                }
            }
        }
        if (ceilingHit)
        {
            velocity.y = -1;
        }
        controller.Move(velocity * Time.deltaTime);
    }
    void GroundCheck()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if(isGrounded && velocity.y < 0)
        {
            velocity.y = 0f;
        }
    }
    void CeilingCheck()
    {
        ceilingHit = Physics.CheckSphere(ceilingCheck.position, groundDistance, groundMask);
    }
    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded && !toggleCrouch)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -5f * gravity);
        }
        velocity.y += gravity * Time.deltaTime;
    }
    void Crouch()
    {
        if(Input.GetButtonDown("Crouch"))
        {
            if (!ceilingHit)
            {
                toggleCrouch = !toggleCrouch;
                CheckCrouch();
            }
            else if (ceilingHit && !toggleCrouch)
            {
                toggleCrouch = !toggleCrouch;
                CheckCrouch();
            }
        }
    }

    void Run()
    {
        if(Input.GetButton("Run") && !toggleCrouch)
        {
            speed = runSpeed;
        }
        else if(toggleCrouch)
        {
            speed = crouchSpeed;
        }
        else
        {
            speed = playerOriginalSpeed;
        }
    }

    void CheckCrouch()
    {
        if(toggleCrouch == true)
        {
            playerTransposer.m_FollowOffset.y = cameraYCrouchOffset;
            controller.height = CrouchHeight;
            speed = crouchSpeed;
        }
        else
        {
            if(!ceilingHit)
            {
                playerTransposer.m_FollowOffset.y = cameraYOriginal;
                controller.height = playerOriginalHeight;
                speed = playerOriginalSpeed;
            }
        }
    }
    
    public void ToggleMovement()
    {
        toggleMovement = !toggleMovement;
    }

    public void SavePlayer()
    {
        playerSavedInventory.hasAxe = playersCurrentInventory.hasAxe;
        playerSavedInventory.hasPistol = playersCurrentInventory.hasPistol;
        playerSavedInventory.pistolCurrentAmmo = playersCurrentInventory.pistolCurrentAmmo.initialValue;
        playerSavedInventory.pistolCurrentCapacity = playersCurrentInventory.pistolCurrentCapacity.initialValue;
        playerSavedInventory.hasShotgun = playersCurrentInventory.hasShotgun;
        playerSavedInventory.shotgunCurrentAmmo = playersCurrentInventory.shotgunCurrentAmmo.initialValue;
        playerSavedInventory.shotgunCurrentCapacity = playersCurrentInventory.shotgunCurrentCapacity.initialValue;
        playerSavedInventory.hasSMG = playersCurrentInventory.hasSMG;
        playerSavedInventory.sMGCurrentAmmo = playersCurrentInventory.sMGCurrentAmmo.initialValue;
        playerSavedInventory.sMGCurrentCapacity = playersCurrentInventory.sMGCurrentCapacity.initialValue;
        playerSavedInventory.hasGrenade = playersCurrentInventory.hasGrenade;
        playerSavedInventory.grenadeCurrentAmmo = playersCurrentInventory.grenadeCurrentAmmo.initialValue;

        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        Inventory currentPlayerInventory = FindObjectOfType<Inventory>();
        
        currentPlayerHealth = data.health;

        controller = this.gameObject.GetComponent<CharacterController>();
        Vector3 position;
        position.x = data.playerPosition[0];
        position.y = data.playerPosition[1];
        position.z = data.playerPosition[2];

        controller.enabled = false;
        controller.transform.position = position;
        controller.enabled = true;


        playersCurrentInventory.hasAxe = data.playerInventory.hasAxe;
        if(data.playerInventory.hasAxe)
        {

        }
        playersCurrentInventory.hasPistol = data.playerInventory.hasPistol;
        if(data.playerInventory.hasPistol)
        {
            currentPlayerInventory.ActivatePistol();
        }
        else
        {
            currentPlayerInventory.DeactivatePistol();
        }
        playersCurrentInventory.pistolCurrentAmmo.initialValue = data.playerInventory.pistolCurrentAmmo;
        playersCurrentInventory.pistolCurrentCapacity.initialValue = data.playerInventory.pistolCurrentCapacity;
        playersCurrentInventory.hasShotgun = data.playerInventory.hasShotgun;
        if(data.playerInventory.hasShotgun)
        {
            currentPlayerInventory.ActivateShotgun();
        }
        else
        {
            currentPlayerInventory.DeactivateShotgun();
        }
        playersCurrentInventory.shotgunCurrentAmmo.initialValue = data.playerInventory.shotgunCurrentAmmo;
        playersCurrentInventory.shotgunCurrentCapacity.initialValue = data.playerInventory.shotgunCurrentCapacity;
        playersCurrentInventory.hasSMG = data.playerInventory.hasSMG;
        playersCurrentInventory.sMGCurrentAmmo.initialValue = data.playerInventory.sMGCurrentAmmo;
        playersCurrentInventory.sMGCurrentCapacity.initialValue = data.playerInventory.sMGCurrentCapacity;
        playersCurrentInventory.hasGrenade = data.playerInventory.hasGrenade;
        playersCurrentInventory.grenadeCurrentAmmo.initialValue = data.playerInventory.grenadeCurrentAmmo;
    }
}
