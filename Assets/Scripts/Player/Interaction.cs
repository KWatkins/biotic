using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{
    private GameObject raycastedObject;

    [SerializeField] private int rayLength = 10;
    [SerializeField] private LayerMask layerMaskInteract;

    [SerializeField] private Image uiCrosshair;

    private bool setNormal;


    float distance;
    Rigidbody holdableRB;
    [SerializeField] bool isHolding = false;

    private GameObject pickupGameObject;
    [SerializeField] float throwForce = 600;
    [SerializeField] float holdDistance = 1.5f;
    private float collisionTypeChangeTime = 10;

    private int HoldableItemLayer = 15;
    private int GroundLayer = 6;

    Vector3 objectPos;

    public bool checkForHookups = false;
    private GameObject holdableGO;

    GameObject canvasGO;
    GameObject uiCrosshairGO;
    private void OnEnable()
    {

    }
    private void Update()
    {
        if (pickupGameObject == null || canvasGO == null || uiCrosshairGO == null)
        {
            checkForHookups = true;
        }

        if (checkForHookups)
        {
            pickupGameObject = GameObject.FindGameObjectWithTag("Pickup");
            canvasGO = GameObject.FindGameObjectWithTag("Canvas");
            if (canvasGO != null)
            {
                uiCrosshairGO = canvasGO.transform.Find("CrossHairs").gameObject;
                uiCrosshair = uiCrosshairGO.GetComponent<Image>();
            }
            if (pickupGameObject != null && canvasGO != null && uiCrosshairGO != null)
            {
                checkForHookups = false;
            }
        }
    }
    private void LateUpdate()
    {
        

        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);


        if (Input.GetButtonUp("Interact"))
        {
            isHolding = false;
        }

        if (Physics.Raycast(transform.position, fwd, out hit, rayLength, layerMaskInteract.value))
        {
            if (hit.collider.CompareTag("Interactable"))
            {
                setNormal = false;
                raycastedObject = hit.collider.gameObject;
                CrossHairActive();

                if (Input.GetButtonDown("Interact"))
                {
                    hit.collider.gameObject.GetComponent<Interactable>().checkActivation = true;
                    hit.collider.gameObject.GetComponent<Interactable>().PlayInteraction();

                }
            }
            if (hit.collider.CompareTag("Holdable") && !isHolding)
            {
                holdableRB = hit.collider.gameObject.GetComponent<Rigidbody>();
                holdableGO = hit.collider.gameObject;
                if (distance >= holdDistance)
                {
                    hit.collider.gameObject.layer = GroundLayer;
                    holdableRB.transform.SetParent(null);
                    isHolding = false;
                }



                if (Input.GetButtonDown("Interact"))
                {
                    
                    if (distance <= holdDistance)
                    {
                        isHolding = true;
                        holdableRB.useGravity = false;
                    }
                }
            }
            
        }
        else
        {
            setNormal = true;

            if (setNormal == true)
            {
            CrossHairNormal();
            }
        }

        if (isHolding == true)
        {
            distance = Vector3.Distance(holdableRB.transform.position, pickupGameObject.transform.position);
            holdableGO.layer = HoldableItemLayer;
            collisionTypeChangeTime = 10f;
            holdableRB.angularVelocity = Vector3.zero;
            holdableRB.collisionDetectionMode = CollisionDetectionMode.Continuous;
            //holdableRB.transform.SetParent(pickupGameObject.transform);
            if (Input.GetMouseButtonDown(1))
            {
                holdableRB.AddForce(pickupGameObject.transform.forward * throwForce);
                isHolding = false;
            }
        }
        else
        {
            distance = 0;
            if(holdableGO != null)
            {
                holdableGO.layer = GroundLayer;
            }

            if(holdableRB != null)
            {
                objectPos = holdableRB.transform.position;
                holdableRB.GetComponent<Rigidbody>().useGravity = true;
                holdableRB.transform.position = objectPos;
               // holdableRB.transform.SetParent(null);
            }

            if (collisionTypeChangeTime > 0)
            {
                collisionTypeChangeTime -= Time.deltaTime;
            }
            else
            {
                if(holdableRB != null)
                {
                holdableRB.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Discrete;
                }
            }
        }

    }

    private void FixedUpdate()
    {
        if (isHolding)
        {
            holdableRB.velocity = (pickupGameObject.transform.position - holdableRB.transform.position) * 20f;
        }
    }
    void CrossHairActive()
    {
        uiCrosshair.color = Color.red;
        setNormal = false;
    }

    void CrossHairNormal()
    {
        if (uiCrosshair != null)
        {
            uiCrosshair.color = Color.white;
        }

    }

}
