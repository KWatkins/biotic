using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventoryData : ScriptableObject
{
    public bool hasAxe;
    public bool hasPistol;
    public IntValue pistolCurrentAmmo;
    public IntValue pistolMaxAmmo;
    public IntValue pistolCurrentCapacity;
    public IntValue pistolMaxCapacity;
    public bool hasShotgun;
    public IntValue shotgunCurrentAmmo;
    public IntValue shotgunMaxAmmo;
    public IntValue shotgunCurrentCapacity;
    public IntValue shotgunMaxCapacity;
    public bool hasSMG;
    public IntValue sMGCurrentAmmo;
    public IntValue sMGMaxAmmo;
    public IntValue sMGCurrentCapacity;
    public IntValue sMGMaxCapacity;
    public bool hasGrenade;
    public IntValue grenadeCurrentAmmo;
    public IntValue grenadeMaxAmmo;
}
