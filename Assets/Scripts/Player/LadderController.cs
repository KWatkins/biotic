using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : MonoBehaviour
{
    [SerializeField] private float climbingSpeed;
    [SerializeField] private CharacterController characterController;
    [SerializeField] private PlayerController playerController;

    private bool isClimbingLadder = false;
    private bool isCollidingWithLadder = false;

    private Transform ladderTransform;
    private float originalGravity;
    private float noGravity = 0;

    private void Start()
    {
        originalGravity = playerController.gravity;
        characterController = GetComponent<CharacterController>();
        playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (isClimbingLadder)
        {
            if (!isCollidingWithLadder || Input.GetButtonDown("Jump"))
            {
                ToggleLadderClimbing(false);
            }
        }
        else if (isCollidingWithLadder)
        {
            if (Vector3.Dot(ladderTransform.right, transform.forward) >= 0.9f &&
                (Input.GetAxis("Vertical") > .0f || !characterController.isGrounded))
            {
                ToggleLadderClimbing(true);
            }
        }
    }

    private void FixedUpdate()
    {
        if (isClimbingLadder)
        {
            playerController.gravity = noGravity;
            transform.Translate(Vector3.up * Input.GetAxis("Vertical") *
                climbingSpeed * Time.deltaTime);
        }
    }

    private void ToggleLadderClimbing(bool isEnabled)
    {
        isClimbingLadder = isEnabled;
        playerController.ToggleMovement();
        playerController.gravity = originalGravity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ladder"))
        {
            isCollidingWithLadder = true;
            ladderTransform = other.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ladder"))
        {
            isCollidingWithLadder = false;
        }
    }
}
