using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    [SerializeField] private float mouseSensitivity = 100f;
    [SerializeField] private Transform playerBody;

    private float vertiRecoil;
    private float horiRecoil;
    [SerializeField] private float recoilSpeed;


    float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float mouseX =  vertiRecoil + Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = horiRecoil + Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        horiRecoil -= recoilSpeed * Time.deltaTime;
        vertiRecoil -= recoilSpeed * Time.deltaTime;
        if (horiRecoil < 0)
        {
            horiRecoil = 0;
        }

        if (vertiRecoil < 0)
        {
            vertiRecoil = 0;
        }

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    public void AddRecoil(float up, float side)
    {
        if(horiRecoil <= side)
        {
            vertiRecoil += up;
            horiRecoil += side;
        }

    }
}
