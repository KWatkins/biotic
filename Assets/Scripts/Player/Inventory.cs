using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    public InventoryData currentInventory;
    public WeaponManager currentWeaponManager;

    [SerializeField]
    private GameObject axePrefab;
    [SerializeField]
    private GameObject pistolPrefab;
    [SerializeField]
    private GameObject shotgunPrefab;
    [SerializeField]
    private GameObject smgPrefab;
    [SerializeField]
    private GameObject grenadePrefab;
    [SerializeField]
    GameObject axe;
    [SerializeField]
    GameObject pistol;
    [SerializeField]
    GameObject shotgun;
    [SerializeField]
    GameObject sMG;
    [SerializeField]
    GameObject grenade;

    private void Start()
    {
        if(currentInventory.hasPistol)
        {
            //currentWeaponManager.weapons[0].gameObject.SetActive(true); //Need check if active someway else
        }
    }

    private void Update()
    {
        if(!currentInventory.hasPistol)
        {
            DeactivatePistol();
        }
        if (!currentInventory.hasAxe)
        {
            
        }
        if (!currentInventory.hasShotgun)
        {
            DeactivateShotgun();
        }
        if (!currentInventory.hasSMG)
        {
            DeactivateSMG();
        }
        if (!currentInventory.hasGrenade)
        {

        }
    }
    public void EmptyWeaponList()
    {
        currentWeaponManager = FindObjectOfType<WeaponManager>();
        currentWeaponManager.weapons.Clear();
    }

    public void ActivatePistol()
    {
        if(!currentWeaponManager.weapons.Contains(pistolPrefab.GetComponent<Pistol>()))
        {
            currentWeaponManager.weapons.Add(pistolPrefab.GetComponent<Pistol>());
        }
    }

    public void DeactivatePistol()
    {
        pistol.SetActive(false);
    }

    public void ActivateShotgun()
    {
        if (!currentWeaponManager.weapons.Contains(shotgunPrefab.GetComponent<Shotgun>()))
        {
            currentWeaponManager.weapons.Add(shotgunPrefab.GetComponent<Shotgun>());
        }
    }
    public void DeactivateShotgun()
    {
        shotgun.SetActive(false);
    }

    public void ActivateSMG()
    {
        if (!currentWeaponManager.weapons.Contains(smgPrefab.GetComponent<SMG>()))
        {
            currentWeaponManager.weapons.Add(smgPrefab.GetComponent<SMG>());
        }
    }
    public void DeactivateSMG()
    {
        sMG.SetActive(false);
    }
}
