using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour
{

    [SerializeField] private float amount;
    [SerializeField] private float smoothAmount;
    [SerializeField] private float maxAmount;
    private Vector3 initialPosition;
    private float min;
    private float max;
    private bool isCrouched;

    private PlayerController playerController;

    private bool isRunning;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.localPosition;
    }

    private void OnEnable()
    {
        playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        float movementX = -Input.GetAxis("Mouse X") * amount;
        float movementY = -Input.GetAxis("Mouse Y") * amount;
        movementX = Mathf.Clamp(movementX, -maxAmount, maxAmount);
        movementY = Mathf.Clamp(movementY, -maxAmount, maxAmount);

        Vector3 finalPosition = new Vector3(movementX, movementY, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition + initialPosition, Time.deltaTime * smoothAmount);

        Run();
        
        if(isRunning && playerController.toggleCrouch == false)
        {
            min = transform.localPosition.x + -.006f;
            max = transform.localPosition.x + .006f;
            transform.localPosition = new Vector3(Mathf.PingPong(Time.time * .02f, max - min) + min, transform.localPosition.y, transform.localPosition.z);
        }
    }

    void Run()
    {
        
        if (Input.GetButtonDown("Run"))
        {
            isRunning = true;
        }
        if (Input.GetButtonUp("Run"))
        {
            isRunning = false;
        }
    }

    void checkCrouch()
    {
        isCrouched = playerController.toggleCrouch;
    }
}
