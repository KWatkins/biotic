using UnityEngine;
using UnityEngine.Playables;

public class OnTriggerTimeline : MonoBehaviour
{
    [SerializeField] PlayableDirector[] timeline;
    int current = 0;
    bool startTimelines = false;

    // Use this for initialization
    void Update()
    {
        if(startTimelines == true)
        {
            if (current != timeline.Length)
            {
                timeline[current].Play();

                current = current + 1;
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(current != timeline.Length)
        {
            if (collider.gameObject.tag == "Player")
            {
                startTimelines = true;
            }
        }

    }
}
