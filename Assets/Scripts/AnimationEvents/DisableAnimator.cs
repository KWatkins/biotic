using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAnimator : MonoBehaviour
{

    private void Start()
    {
        this.gameObject.GetComponent<Animator>().enabled = true;
    }
    public void EnableThisAnimator()
    {
        this.gameObject.GetComponent<Animator>().enabled = true;
    }

    public void DisableThisAnimator()
    {
        this.gameObject.GetComponent<Animator>().enabled = false;
    }

}
