using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuseBox : MonoBehaviour
{

    [SerializeField] GameObject stationaryFuse;
    [SerializeField] GameObject sparks;
    private bool isPower;
    private Rigidbody rb;

    private void Update()
    {
        if(isPower)
        {
            IsPowered();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fuse"))
        {
            rb = other.gameObject.GetComponent<Rigidbody>();
            other.transform.position = stationaryFuse.transform.position;
            other.transform.rotation = stationaryFuse.transform.rotation;

            if (rb != null)
            {
                rb.isKinematic = true;
            }

            sparks.SetActive(true);
            isPower = true;
        }
    }

    void IsPowered()
    {
        MovingObject movingObject = GetComponentInChildren<MovingObject>();

        if (movingObject != null)
        {
            movingObject.isPowered = true;
        }

    }
}
