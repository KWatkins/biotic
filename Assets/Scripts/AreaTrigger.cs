using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour
{

    [SerializeField] bool soundOnEnter;
    [SerializeField] private AudioClip doorOpenSound;
    [SerializeField] private AudioClip doorCloseSound;

    [SerializeField] bool animationOnEnter;
    private Animator anim;

    [SerializeField] bool activateOnEnter;
    [SerializeField] GameObject objectToActivate;

    private void Start()
    {
        if(animationOnEnter)
        {
            anim = this.gameObject.GetComponent<Animator>();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (animationOnEnter)
            {
            anim.SetTrigger("open");
            }
            if(soundOnEnter)
            {
                this.gameObject.GetComponent<AudioSource>().clip = doorOpenSound;
                this.gameObject.GetComponent<AudioSource>().PlayOneShot(doorOpenSound, 1f);
            }
            
            if(activateOnEnter)
            {
                objectToActivate.SetActive(true);
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(animationOnEnter)
            {
            anim.SetTrigger("close");
            }
            if(soundOnEnter)
            {
                this.gameObject.GetComponent<AudioSource>().clip = doorCloseSound;
                this.gameObject.GetComponent<AudioSource>().PlayOneShot(doorCloseSound, 1f);
            }

        }
    }
}
