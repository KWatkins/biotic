using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbHealth : MonoBehaviour
{

    [SerializeField] public enum Limb { LeftArm, Rightarm, Head, Torso, LowerBody }

    public Limb limb;
    public Transform orientation;
    [SerializeField] Health rootHealth;
    [SerializeField] GameObject limbGO;
    [SerializeField] float limbRemainingHealth;
    [SerializeField] Animator thisAnimator;

    [SerializeField] GameObject bloodEffect;

    bool takingDamage;
    bool isDestroyed;

    [SerializeField] Collider[] collidersToDeactivate;

    [SerializeField] LayerMask isGround;
    [SerializeField] GameObject ceilingBlood;

    // Start is called before the first frame update
    void Start()
    {
        if (rootHealth == null)
        {
            rootHealth = transform.root.GetComponent<Health>();
        }

        if (thisAnimator == null)
        {
            thisAnimator = GetComponentInParent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(float amount)
    {
        if (!takingDamage)
        {
            takingDamage = true;
            limbRemainingHealth -= amount;

            if (limbRemainingHealth <= 0)
            {
                LimbDie();
            }
            else
            {
                rootHealth.TakeDamage(amount);
            }
            takingDamage = false;
        }

    }

    void LimbDie()
    {

        foreach (Collider colliders in collidersToDeactivate)
        {
            colliders.enabled = false;
        }

        limbGO.SetActive(false);

        if (bloodEffect != null)
        {
            bloodEffect.SetActive(true);
        }

        if (!isDestroyed && !rootHealth.isDead)
        {
            switch (limb)
            {
                case Limb.Head:
                    rootHealth.deathAnim = Health.DeathAnim.Head;
                    CeilingBloodDropEffect();
                    break;
                case Limb.Torso:
                    rootHealth.deathAnim = Health.DeathAnim.Torso;
                    break;
                case Limb.LowerBody:
                    rootHealth.deathAnim = Health.DeathAnim.LowerBody;
                    break;
                case Limb.Rightarm:
                    rootHealth.deathAnim = Health.DeathAnim.Rightarm;
                    break;
                case Limb.LeftArm:
                    rootHealth.deathAnim = Health.DeathAnim.LeftArm;
                    break;
            }
            rootHealth.Die();
        }
        else if (!isDestroyed && rootHealth.isDead)
        {
            switch (limb)
            {
                case Limb.Head:

                    break;
                case Limb.Torso:

                    break;
                case Limb.LowerBody:
                    rootHealth.GoRagdoll();
                    break;
                case Limb.Rightarm:

                    break;
                case Limb.LeftArm:

                    break;
            }
            rootHealth.Die();
        }
        isDestroyed = true;
    }

    void CeilingBloodDropEffect()
    {
        RaycastHit rayHit;
        if (Physics.Raycast(orientation.position, Vector3.up, out rayHit, isGround))
        {
            Debug.Log("Hit ground");
            Instantiate(ceilingBlood, rayHit.point + new Vector3(0, .001f, 0), transform.rotation);
        }
    }
}