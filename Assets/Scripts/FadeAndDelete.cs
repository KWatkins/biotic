using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAndDelete : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(FadeOutObject(this.gameObject));
	}

    // Update is called once per frame
    void Update()
    {

    }

	public IEnumerator FadeOutObject(GameObject objToFade)
	{
		yield return new WaitForSeconds(2f);
		// Get the mesh renderer of the object
		MeshRenderer meshRenderer = objToFade.GetComponent<MeshRenderer>();

		// Get the color value of the main material
		Color color = meshRenderer.materials[0].color;

		// While the color's alpha value is above 0
		while (color.a > 0)
		{
			// Reduce the color's alpha value
			color.a -= 0.1f;

			// Apply the modified color to the object's mesh renderer
			meshRenderer.materials[0].color = color;

			// Wait for the frame to update
			yield return new WaitForSeconds(.1f);
		}

		if(color.a <= 0)
        {
			this.gameObject.SetActive(false);
        }
	}
}
