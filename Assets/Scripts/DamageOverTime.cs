using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTime : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        Health targetHealth = other.transform.GetComponent<Health>();
        if (targetHealth != null)
        {
            targetHealth.damageOverTime();
        }
    }
}
