using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{

    public bool isPowered;
    [SerializeField] bool isActivated;

    [SerializeField] GameObject[] waypoints;
    public int currentWaypoint = 0;
    [SerializeField] float speed;
    float WPradius = 1;

    [SerializeField] bool powerOnEnter;

    public bool hasActivated = false;


    private bool playerInTrigger;

    
    private void FixedUpdate()
    {
        if(isPowered)
        {
            if(Vector3.Distance(waypoints[currentWaypoint].transform.position, transform.position) < WPradius)
            {
                currentWaypoint = currentWaypoint + 1;
                if (currentWaypoint >= waypoints.Length)
                {
                    
                }
            }
            transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWaypoint].transform.position, Time.deltaTime * speed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {

            other.gameObject.transform.parent = gameObject.transform;

            if(powerOnEnter)
            {
                StartCoroutine(ActivateCoroutine());
            }
        }

        if (other.CompareTag("Holdable"))
        {

            other.gameObject.transform.parent = gameObject.transform;

            if (powerOnEnter)
            {
                StartCoroutine(ActivateCoroutine());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.transform.parent = null;
        }

        if (other.CompareTag("Holdable"))
        {
            other.transform.parent = null;
        }
    }

    IEnumerator ActivateCoroutine()
    {
        if (hasActivated == false)
        {
            yield return new WaitForSeconds(2);
            isPowered = true;
            hasActivated = true;
        }

    }
}
