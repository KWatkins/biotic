using UnityEngine;
using System.Collections;

public abstract class Factory
{
    public abstract void CreateObject();
}

public class PistolClip : Factory
{
    public override void CreateObject()
    {
        //get the prefab
        //instantiate it at the position it was saved at
    }
}

public class ItemFactory
{
    public Factory GetFactory(string itemID)
    {
        switch (itemID)
        {
            case "PistolClipID":
                return new PistolClip();
            default:
                return null;
        }
    }
}