using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveForceNoDmg : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float radius = 1f;
    [SerializeField] private float power = 8f;
    private float upforce = 1.0f;
    bool hasExploded = false;
    Vector3 explosionPos;


    void Start()
    {
        explosionPos = transform.position;
        if (!hasExploded)
        {

            Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
            foreach (Collider hit in colliders)
            {
                RaycastHit RayHit;
                if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out RayHit, Mathf.Infinity))
                {

                }
            }


            hasExploded = true;
        }
    }

    private void Update()
    {
        if (hasExploded)
        {
            Destroy(this);
        }
    }

    private void FixedUpdate()
    {
        Collider[] collidersForce = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in collidersForce)
        {
            RaycastHit RayHit;
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out RayHit, Mathf.Infinity))
            {
                if (RayHit.collider == hit)
                {
                    Rigidbody[] rb = hit.GetComponents<Rigidbody>();
                    if (rb != null)
                    {
                        foreach (Rigidbody rigidbody in rb)
                        {
                            rigidbody.AddExplosionForce(power, explosionPos, radius, upforce, ForceMode.Impulse);
                            rigidbody.AddForce(-RayHit.normal * power);
                        }
                    }
                    ImpactReciever impactRecievers = hit.GetComponent<ImpactReciever>();
                    if (impactRecievers != null)
                    {
                        Vector3 dir = hit.transform.position - this.transform.position;
                        impactRecievers.AddImpact(dir, 60f);
                    }
                }

            }
        }
    }

    IEnumerator WaitAddForce(Rigidbody rb, RaycastHit RayHit)

    {
        yield return new WaitForSeconds(1f);
        rb.AddForce(-RayHit.normal * power);
    }

}
