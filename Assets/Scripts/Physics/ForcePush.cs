using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcePush : MonoBehaviour
{
    // Start is called before the first frame update

    private float radius = 5f;
    private float power = 1f;
    private float upforce = 1.0f;

    void Start()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(power, explosionPos, radius, upforce, ForceMode.Impulse);
            }
            ImpactReciever impactRecievers = hit.GetComponent<ImpactReciever>();
            if (impactRecievers != null)
            {
                Vector3 dir = hit.transform.position - this.transform.position;
                impactRecievers.AddImpact(dir, 60f);
            }
        }

        Destroy(this, 1f);
    }
}
