using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveForce : MonoBehaviour
{
    // Start is called before the first frame update

    private float radius = 5f;
    private float power = 100f;
    private float upforce = 1.0f;
    bool hasExploded = false;
    Vector3 explosionPos;


    void Start()
    {
        explosionPos = transform.position;
        if (!hasExploded)
        {

            Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
            foreach (Collider hit in colliders)
            {
                RaycastHit RayHit;
                if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out RayHit, Mathf.Infinity))
                {
                    if (RayHit.collider == hit)
                    {
                        Health healthInRangeInParent = hit.GetComponentInParent<Health>();

                        if (healthInRangeInParent != null)
                        {
                            if (healthInRangeInParent.isPlayer)
                            {
                                healthInRangeInParent.TakeDamage(50f);
                            }
                            else if (healthInRangeInParent.isEnemy)
                            {
                                healthInRangeInParent.TakeDamage(100f);
                            }
                            else
                            {
                                healthInRangeInParent.TakeDamage(50f);
                            }
                        }
                    }


                }
            }


            hasExploded = true;
        }
    }

    private void Update()
    {
        if (hasExploded)
        {
            Destroy(this);
        }
    }

    private void LateUpdate()
    {
        Collider[] collidersForce = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in collidersForce)
        {
            RaycastHit RayHit;
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out RayHit, Mathf.Infinity))
            {
                if (RayHit.collider == hit)
                {
                    Rigidbody[] rb = hit.GetComponents<Rigidbody>();
                    if (rb != null)
                    {
                        foreach (Rigidbody rigidbody in rb)
                        {
                            rigidbody.AddExplosionForce(power, explosionPos, radius, upforce, ForceMode.Impulse);
                            rigidbody.AddForce(-RayHit.normal * power);
                        }
                    }
                    ImpactReciever impactRecievers = hit.GetComponent<ImpactReciever>();
                    if (impactRecievers != null)
                    {
                        Vector3 dir = hit.transform.position - this.transform.position;
                        impactRecievers.AddImpact(dir, 60f);
                    }
                }

            }
        }
    }

    IEnumerator WaitAddForce(Rigidbody rb, RaycastHit RayHit)

    {
        yield return new WaitForSeconds(1f);
        rb.AddForce(-RayHit.normal * power);
    }

}
