using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string currentLevel;

    public float health;
    public float[] playerPosition;

    public bool[] isPickedUp;
    public SerializableVector3[] levelPickupVector3Serializable;

    public SaveInventoryItem playerInventory;

    public List<WorldItemData> worldItemData;

    public PlayerData(LevelManager currentLevelManager)
    {
        worldItemData = currentLevelManager.worldItemData;
    }

    public PlayerData(SceneController ActiveSceneController)
    {
        currentLevel = ActiveSceneController.currentLevel;
    }

    public PlayerData (PlayerController player)
    {
        health = player.currentPlayerHealth;

        playerPosition = new float[3];
        playerPosition[0] = player.transform.position.x;
        playerPosition[1] = player.transform.position.y;
        playerPosition[2] = player.transform.position.z;

        playerInventory = player.playerSavedInventory;
    }

}
