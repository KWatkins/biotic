using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    [SerializeField]
    Signal loadLevelSignal;
    Signal transitionLevelSignal;

    public string currentLevel;


    public bool loadlevel = false;
    public bool transitionLevel = false;

    private void Update()
    {

        if(currentLevel == "")
        {
            currentLevel = SceneManager.GetActiveScene().name;
        }
    }


    public void SaveLevel()
    {
        SaveSystem.SaveSceneManager(this);
    }

    public void LoadLevel()
    {
        transitionLevel = false;
        loadlevel = true;
        PlayerData data = SaveSystem.LoadSceneManager();
        SceneManager.LoadScene(data.currentLevel);
    }

    public void TransitionLevel()
    {
        loadlevel = false;
        transitionLevel = true;

    }
}
