using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldItemData
{
    public string uniqueID;
    public int objectID = 0;
    public SerializableVector3 currentPosition;
    public SerializableQuaternion currentRotation;
    public bool isNotActive;

    public WorldItemData(string thisUniqueID, int thisObjectID, SerializableVector3 thisCurrentPosition, SerializableQuaternion thisCurrentRotation, bool thisIsNotActive)
    {
        uniqueID = thisUniqueID;
        objectID = thisObjectID;
        currentPosition = thisCurrentPosition;
        currentRotation = thisCurrentRotation;
        isNotActive = thisIsNotActive;
    }
}
