using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveInventoryItem
{
    public bool hasAxe;
    public bool hasPistol;
    public int pistolCurrentAmmo;
    public int pistolMaxAmmo;
    public int pistolCurrentCapacity;
    public int pistolMaxCapacity;
    public bool hasShotgun;
    public int shotgunCurrentAmmo;
    public int shotgunMaxAmmo;
    public int shotgunCurrentCapacity;
    public int shotgunMaxCapacity;
    public bool hasSMG;
    public int sMGCurrentAmmo;
    public int sMGMaxAmmo;
    public int sMGCurrentCapacity;
    public int sMGMaxCapacity;
    public bool hasGrenade;
    public int grenadeCurrentAmmo;
    public int grenadeMaxAmmo;

}