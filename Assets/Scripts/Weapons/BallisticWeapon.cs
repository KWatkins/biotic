using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;

using System.Linq;

public class BallisticWeapon : MonoBehaviour
{
    [SerializeField] MouseLook m_MouseLook;
    [SerializeField] private Camera fpsCam;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private float impactForce;
    [SerializeField] private float fireRate = 15;


    [SerializeField] public bool isSMG;
    [SerializeField] public bool isPistol;
    [SerializeField] public bool isShotgun;

    [SerializeField] int maxAmmo;       // bullets per magazine
    [SerializeField] public int currentAmmo;  //current bullets in our magazine
    [SerializeField] int maxCapacity;  // Bullet limit
    [SerializeField] public int currentCapacity; //total bullets
    [SerializeField] int reloadTime;
    private bool isReloading = false;

    [SerializeField] private TMP_Text ammoText;
    [SerializeField] private TMP_Text maxCapacityText;

    [SerializeField] private GameObject bloodEffect;
    [SerializeField] private GameObject fleshWound;

    [SerializeField] private GameObject impactEffect;
    [SerializeField] private GameObject impactEffectDamage;


    [SerializeField] private AudioClip shootSound;
    [SerializeField] private AudioClip reloadSound;

    [SerializeField] private GameObject bloodSplatterEffect;

    [SerializeField] private float vertiRecoil;
    [SerializeField] private float horiRecoil;
    [SerializeField] private Animator weaponAnimator;

    [SerializeField] private Cinemachine.CinemachineVirtualCamera playerVirtualCamera;
    private float playerOriginalFOV;
    private float recoilFOV;


    private float nextTimeToFire = 0f;
    private AudioSource weaponFireSound;

    [SerializeField] LayerMask targetLayerMask;
    [SerializeField] LayerMask bloodLayerMask;

    private void Start()
    {
        weaponFireSound = GetComponent<AudioSource>();
        playerOriginalFOV = playerVirtualCamera.m_Lens.FieldOfView;
        recoilFOV = playerOriginalFOV + 1f;

        GameObject canvasGO = GameObject.FindGameObjectWithTag("Canvas");
        GameObject ammoTextGO = canvasGO.transform.Find("AmmoHolder").transform.Find("PistolAmmo").transform.Find("AmmoText").gameObject;
        ammoText = ammoTextGO.GetComponent<TMP_Text>();
        GameObject MaxammoTextGO = canvasGO.transform.Find("AmmoHolder").transform.Find("PistolAmmo").transform.Find("TotalAmmoText").gameObject;
        maxCapacityText = MaxammoTextGO.GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        isReloading = false;
        //set animator to false
    }

    void FixedUpdate()
    {
        Debug.Log("Still here");
        updateAmmo();
        FOVCheck();
        if (isReloading)
            return;

        if(currentAmmo < maxAmmo)
        {
            if (currentAmmo <= 0 && currentCapacity > 0 || (Input.GetButtonDown("Reload") && currentCapacity > 0))
            {
                StartCoroutine(Reload());
                return;
            }
        }


        if(currentAmmo > 0)
        {
            if (isSMG)
            {
                if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
                {
                    nextTimeToFire = Time.time + 1f / fireRate;
                    Shoot();
                }
            }
            else
            {
                if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
                {
                    nextTimeToFire = Time.time + 1f / fireRate;
                    Shoot();
                }
            }
        }

        
    }
    private void updateAmmo()
    {
        if (currentCapacity > maxCapacity)
        {
            currentCapacity = maxCapacity;
        }

        if (currentCapacity < 0)
        {
            currentCapacity = 0;
        }
        ammoText.text = currentAmmo + "";
        maxCapacityText.text = currentCapacity + "";
    }
    IEnumerator Reload()
    {
        PlayReloadSound();
        weaponAnimator.SetTrigger("isReloading");
        if(currentCapacity > 0)
        {
            isReloading = true;

            yield return new WaitForSeconds(reloadTime - .25f);
            //reload animation
            yield return new WaitForSeconds(.25f);

            int bulletsToLoad = maxAmmo - currentAmmo;
            int bulletsToDeduct = (currentCapacity >= bulletsToLoad) ? bulletsToLoad : currentCapacity;

            currentCapacity -= bulletsToDeduct;
            currentAmmo += bulletsToDeduct;

            isReloading = false;
        }
    }

    void Shoot()
    {
        currentAmmo--;
        weaponAnimator.SetTrigger("isFiring");
        m_MouseLook.AddRecoil(horiRecoil / 6, vertiRecoil / 6);
        CameraRecoil();
        muzzleFlash.Play();
        PlayShootSound();

        RaycastHit[] hitArray;
        hitArray = Physics.RaycastAll(fpsCam.transform.position, fpsCam.transform.forward, range, targetLayerMask).OrderBy(h => h.distance).ToArray();

        if (hitArray[0].collider.gameObject.layer == 12)
        {
            Health targetHealth = hitArray[0].collider.gameObject.GetComponentInParent<Health>();

            if (targetHealth != null)
            {
                targetHealth.TakeDamage(damage);
            }

            GameObject bulletHole = Instantiate(fleshWound, hitArray[0].point, Quaternion.LookRotation(hitArray[0].normal));
            bulletHole.transform.SetParent(hitArray[0].transform);

            Rigidbody enemyHit = hitArray[0].transform.GetComponentInChildren<Rigidbody>();
            Rigidbody[] childrenRB = hitArray[0].transform.GetComponentsInChildren<Rigidbody>();
            for (int j = 0; j < childrenRB.Length; j++)
            {
                childrenRB[j].AddExplosionForce(700f, this.transform.position, 10f);
            }

            enemyHit.AddExplosionForce(400f, hitArray[0].point, 400f);

            for (int i = 1; i < hitArray.Length; i++)
            {
                if (hitArray[i].collider.gameObject.layer == 6)
                {
                    GameObject bloodGO = Instantiate(bloodSplatterEffect, hitArray[i].point, Quaternion.LookRotation(hitArray[i].normal));
                    bloodGO.transform.SetParent(hitArray[i].transform);
                    Debug.DrawRay(hitArray[i].point, -hitArray[i].normal, Color.red, 5.0f);
                    Destroy(bloodGO, 60f);
                }
            }
        }

        if (hitArray[0].collider.gameObject.layer == 6)
        {
            Health targetHealth = hitArray[0].collider.gameObject.GetComponentInParent<Health>();

            if (targetHealth != null)
            {
                targetHealth.TakeDamage(damage);
            }

            GameObject impactGO = Instantiate(impactEffect, hitArray[0].point, Quaternion.LookRotation(hitArray[0].normal));
            impactGO.transform.SetParent(hitArray[0].transform);
            GameObject bulletHole = Instantiate(impactEffectDamage, hitArray[0].point, Quaternion.LookRotation(hitArray[0].normal));
            bulletHole.transform.SetParent(hitArray[0].transform);
            Rigidbody impactHit = hitArray[0].transform.GetComponentInChildren<Rigidbody>();

            if(impactHit != null)
            {
                impactHit.AddExplosionForce(impactForce, hitArray[0].point, 0f);
            }

            Destroy(impactGO, 2f);
            Destroy(bulletHole, 10f);

        }
    }

    private void PlayShootSound()
    {
        weaponFireSound.clip = shootSound;
        weaponFireSound.PlayOneShot(shootSound, 1f);
    }

    private void PlayReloadSound()
    {
        weaponFireSound.clip = reloadSound;
        weaponFireSound.Play();
    }

    private void CameraRecoil()
    {
        playerVirtualCamera.m_Lens.FieldOfView = recoilFOV;
    }
    
    private void FOVCheck()
    {
        if (playerVirtualCamera.m_Lens.FieldOfView != playerOriginalFOV)
        {
            playerVirtualCamera.m_Lens.FieldOfView = Mathf.Lerp(playerOriginalFOV, recoilFOV, Time.deltaTime /4 );
        }
    }

    public void AddAmmo(int ammoToAdd)
    {
        currentCapacity += ammoToAdd;
    }

}
