using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WeaponManager : MonoBehaviour
{
    [SerializeField]
    private GameObject hitMarker;
    [SerializeField]
    public List<Weapon> weapons = new List<Weapon>();
    private float firingSince;
    private int currentWeaponIndex = 0;
    [SerializeField]
    BoolValue idleBool;

    int currentWeaponCount;
    public float WeaponRange
    {
        get
        {
            return weapons[currentWeaponIndex].Range;
        }
    }

    public void ClearWeaponList()
    {
        weapons.Clear();
    }

    private void Start()
    {
        currentWeaponCount = weapons.Count;
    }
    private void Update()
    {
        if(weapons.Count == 0)
        {
            idleBool.boolValue = true;
        }
    }

    private void LateUpdate()
    {
        if(weapons.Count != 0)
        {
            OnWeaponFire();
            OnWeaponChange();
        }
        if(weapons.Count != currentWeaponCount)
        {
            weapons[currentWeaponIndex].gameObject.SetActive(false);
            weapons[currentWeaponCount].gameObject.SetActive(true);
            currentWeaponCount = weapons.Count;
            currentWeaponIndex = currentWeaponCount -1;
        }
    }
  
    public void ActivateManager()
    {
        this.gameObject.SetActive(true);
    }
    private void OnWeaponChange()
    {
        if(idleBool.boolValue == true)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > .0f)
            {
                weapons[currentWeaponIndex].gameObject.SetActive(false);
                currentWeaponIndex = (currentWeaponIndex + 1) % weapons.Count;
                weapons[currentWeaponIndex].gameObject.SetActive(true);
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < .0f)
            {
                weapons[currentWeaponIndex].gameObject.SetActive(false);

                if (currentWeaponIndex == 0)
                {
                    currentWeaponIndex = weapons.Count - 1;
                }
                else
                {
                    currentWeaponIndex = (currentWeaponIndex - 1) % weapons.Count;
                }

                weapons[currentWeaponIndex].gameObject.SetActive(true);
            }
        }
    }

    private void OnWeaponFire()
    {
        if (weapons[currentWeaponIndex].IsAutomatic)
        {
            if (Input.GetButton("Fire1"))
            {
                firingSince += Time.deltaTime;
                weapons[currentWeaponIndex].Shoot(firingSince);
            }
            else
            {
                firingSince = .0f;
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))
            {
                weapons[currentWeaponIndex].Shoot(null);
            }
        }
    }
}