using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GunStats : ScriptableObject
{
    public bool isAutomatic;
    public float range;
    public float fireRate;
    public float maxBulletSpreadAngle;
    public float timeUntilMaxSpreadAngle;
    public float damage;
    public float hitForce;
    public LayerMask layerMask;
    public int recoilHorizontal;
    public int recoilVertical;
    public int maxAmmo;
    public int maxCapacity;
    public float reloadTime;
    public float drawTime;
}
