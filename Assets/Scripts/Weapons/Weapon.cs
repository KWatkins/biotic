using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;
public abstract class Weapon : MonoBehaviour
{
    [SerializeField]
    protected GunStats gunStats;
    [SerializeField]
    protected GameObject hitMarker;
    protected AudioSource weaponSound;
    [SerializeField] AudioClip weaponShootSound;
    [SerializeField] AudioClip weaponReloadSound;
    [SerializeField] AudioClip weaponDrawSound;
    protected Camera fpsCam;
    protected Vector3 rayOrigin;
    protected RaycastHit hit;
    protected bool readyToFire = true;
    protected float? firingSince;
    [SerializeField]
    private MouseLook m_mouseLook;
    [SerializeField]
    protected IntValue currentAmmo;
    [SerializeField]
    protected IntValue currentCapacity;
    [SerializeField]
    protected IntValue currentMaxAmmo;
    [SerializeField]
    protected IntValue currentMaxCapacity;
    [SerializeField]
    private TMP_Text ammoText;
    [SerializeField]
    private TMP_Text maxCapacityText;
    [SerializeField]
    private ParticleSystem muzzleFlash;


    Animator weaponAnimator;

    public bool isShotgun = false;
    private bool isReloading = false;
    [SerializeField]
    private BoolValue isIdle;

    bool canvasHookup = false;
    public bool IsAutomatic
    {
        get
        {
            return gunStats.isAutomatic;
        }
    }

    public float Range
    {
        get
        {
            return gunStats.range;
        }
    }

    private void OnDisable()
    {
        canvasHookup = false;
    }
    private void OnEnable()
    {
        if (weaponSound == null)
        {
            weaponSound = GetComponentInParent<AudioSource>();
        }
        weaponAnimator = this.gameObject.GetComponent<Animator>();
        weaponAnimator.SetTrigger("isDrawing");
        weaponSound.PlayOneShot(weaponDrawSound);


    }

    private void Start()
    {

    }

    private void FixedUpdate()
    {

        if (!canvasHookup)
        {
            HookUpWeaponVariables();
            canvasHookup = true;
        }
        if (weaponAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && !isReloading)
        {
            isIdle.boolValue = true;

        }
        else
        {
            isIdle.boolValue = false;
        }
        UpdateAmmo();
        if (currentAmmo.initialValue < gunStats.maxAmmo)
        {
            if (currentAmmo.initialValue <= 0 && currentCapacity.initialValue > 0 && !isReloading ||
               (Input.GetButtonDown("Reload") && currentCapacity.initialValue > 0 && !isReloading))
            {
                StartCoroutine(Reload());
                return;
            }
        }
    }

    private void HookUpWeaponVariables()
    {
        GameObject canvasGO = GameObject.FindGameObjectWithTag("Canvas");
        if (canvasGO != null)
        {
            GameObject ammoTextGO = canvasGO.transform.Find("AmmoHolder").transform.Find("CurrentAmmoCount").transform.Find("AmmoText").gameObject;
            ammoText = ammoTextGO.GetComponent<TMP_Text>();
            GameObject MaxammoTextGO = canvasGO.transform.Find("AmmoHolder").transform.Find("CurrentAmmoCount").transform.Find("TotalAmmoText").gameObject;
            maxCapacityText = MaxammoTextGO.GetComponent<TMP_Text>();
        }



        weaponAnimator.keepAnimatorControllerStateOnDisable = false;
        Debug.Assert(gameObject.layer == LayerMask.NameToLayer("Gun"), "Gun should be on gun layer.");
        fpsCam = GetComponentInParent<Camera>();
        if (weaponSound == null)
        {
            weaponSound = GetComponentInParent<AudioSource>();
        }
        canvasHookup = true;
    }
    public void Shoot(float? firingSince) //Called in the WeaponManager Script
    {
        if (readyToFire && !isReloading && currentAmmo.initialValue >= 1)
        {
            if (weaponShootSound != null)
            {
                if (weaponSound.clip != weaponShootSound)
                {
                    weaponSound.clip = weaponShootSound;
                }
                weaponSound.Play();
                if (weaponAnimator != null)
                {
                    weaponAnimator.SetTrigger("isFiring");
                }
                if (muzzleFlash != null)
                {
                    muzzleFlash.Play();
                }

                currentAmmo.initialValue -= 1;
                this.firingSince = firingSince;
                readyToFire = false;
                Invoke("SetReadyToFire", gunStats.fireRate);

                m_mouseLook.AddRecoil(gunStats.recoilHorizontal / 3, gunStats.recoilVertical / 3);

                if (fpsCam != null)
                {
                    rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0f));
                }

                CalculateShot();

            }
        }
    }

    protected abstract void CalculateShot();

    protected Quaternion CalculateBulletSpread()
    {
        Quaternion fireRotation = Quaternion.LookRotation(transform.forward);
        Quaternion randomRotation = Random.rotation;

        float currentSpread;

        if (firingSince == null)
        {
            currentSpread = gunStats.maxBulletSpreadAngle;
        }
        else
        {
            currentSpread = Mathf.Lerp(.0f, gunStats.maxBulletSpreadAngle, (float)firingSince / gunStats.timeUntilMaxSpreadAngle);
        }

        fireRotation = Quaternion.RotateTowards(fireRotation, randomRotation, currentSpread);
        return fireRotation;
    }

    protected void SetReadyToFire()
    {
        readyToFire = true;
    }

    protected void OnRayCastHit()
    {
        if (hit.rigidbody != null)
        {
            if (!hit.rigidbody.isKinematic)
            {
                hit.rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            }
            hit.rigidbody.AddForce(-hit.normal * gunStats.hitForce);
        }

        if (hit.collider.gameObject.layer == 6) //Ground Layer
        {
            Instantiate(hitMarker, hit.point, Quaternion.LookRotation(hit.normal));
        }

        if (hit.collider.gameObject.layer == 12) //Enemy layer
        {

            if (hit.collider.GetComponent<LimbHealth>() != null)
            {
                if (hit.collider.GetComponent<LimbHealth>())
                {
                    hit.collider.GetComponent<LimbHealth>().TakeDamage(gunStats.damage);
                    //hit.collider.transform.root.GetComponent<Health>().TakeDamage(gunStats.damage);
                    return;
                }
            }
            else if (hit.collider.GetComponentInParent<LimbHealth>() != null)
            {
                if (hit.collider.GetComponentInParent<LimbHealth>())
                {
                    hit.collider.GetComponentInParent<LimbHealth>().TakeDamage(gunStats.damage);
                    //hit.collider.transform.root.GetComponent<Health>().TakeDamage(gunStats.damage);
                    return;
                }
            }
            else if (hit.collider.gameObject.transform.root.GetComponent<Health>() != null)
            {
                hit.collider.gameObject.transform.root.GetComponent<Health>().TakeDamage(gunStats.damage);
            }
            //GameObject bulletWoundDecal = Instantiate(hit.collider.gameObject.GetComponentInParent<Enemy>().bloodDecal, hit.point, Quaternion.LookRotation(hit.normal));
            //bulletWoundDecal.transform.parent = hit.transform;
            // GameObject bloodSpray = Instantiate(hit.collider.gameObject.GetComponentInParent<Enemy>().bloodSpray, hit.point, Quaternion.AngleAxis(Random.Range(0f, 360f), hit.normal) * Quaternion.LookRotation(hit.normal));
            // GameObject bloodPool = Instantiate(hit.collider.gameObject.GetComponent<Enemy>().bloodDecal, hit.point, Quaternion.LookRotation(hit.normal));
        }
        else
        {
            if (hit.collider.gameObject.GetComponent<Health>() != null)
            {
                hit.collider.gameObject.GetComponent<Health>().TakeDamage(gunStats.damage);
            }
        }
    }

    IEnumerator Reload()
    {
        if (isShotgun)
        {
            weaponAnimator.SetTrigger("isReloading");
            isReloading = true;
            while (currentAmmo.initialValue < currentMaxAmmo.initialValue)
            {
                yield return new WaitForSeconds(gunStats.reloadTime - .25f);
                //reload animation
                yield return new WaitForSeconds(.25f);
                int bulletsToLoad = 1;
                int bulletsToDeduct = 1;
                currentCapacity.initialValue -= bulletsToDeduct;
                currentAmmo.initialValue += bulletsToLoad;

            }
            weaponAnimator.SetTrigger("reloadFinish");
            yield return new WaitForSeconds(.9f);
            isReloading = false;
        }
        else
        {
            weaponAnimator.SetTrigger("isReloading");
            if (currentCapacity.initialValue > 0)
            {
                isReloading = true;
                isIdle.boolValue = false;
                PlayReloadSound();
                yield return new WaitForSeconds(gunStats.reloadTime - .25f);
                //reload animation
                yield return new WaitForSeconds(.25f);

                int bulletsToLoad = gunStats.maxAmmo - currentAmmo.initialValue;
                int bulletsToDeduct = (currentCapacity.initialValue >= bulletsToLoad) ? bulletsToLoad : currentCapacity.initialValue;

                currentCapacity.initialValue -= bulletsToDeduct;
                currentAmmo.initialValue += bulletsToDeduct;

                isReloading = false;
            }
        }


    }

    private void PlayReloadSound()
    {
        //weaponSound.clip = weaponReloadSound;
        weaponSound.PlayOneShot(weaponReloadSound);
    }

    private void UpdateAmmo()
    {
        if (currentCapacity.initialValue > currentMaxCapacity.initialValue)
        {
            currentCapacity.initialValue = currentMaxCapacity.initialValue;
        }

        if (currentCapacity.initialValue < 0)
        {
            currentCapacity.initialValue = 0;
        }

        if (ammoText != null)
        {
            ammoText.text = currentAmmo.initialValue + "";
            maxCapacityText.text = currentCapacity.initialValue + "";
        }
        else
        {
            HookUpWeaponVariables();
        }
    }
}
