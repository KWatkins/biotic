using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 class Pistol : Weapon
{
    protected override void CalculateShot()
    {
        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, gunStats.range, gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}
