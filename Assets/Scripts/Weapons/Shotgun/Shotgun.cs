using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Shotgun : Weapon
{
    [SerializeField]
    private int shotsPerBullet = 10;
    
    protected override void CalculateShot()
    {
        firingSince = null;

        for (int i = 0; i < shotsPerBullet; i++)
        {
            if (Physics.Raycast(rayOrigin, CalculateBulletSpread() * Vector3.forward, out hit, gunStats.range, gunStats.layerMask))
            {
                OnRayCastHit();
            }
        }
    }
}
