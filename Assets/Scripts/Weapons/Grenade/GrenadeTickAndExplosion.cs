using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeTickAndExplosion : MonoBehaviour
{
    [SerializeField] float delay;
    [SerializeField] GameObject explosiveForce;

    float countDown;

    bool hasExploded = false;

    private void Start()
    {
        countDown = delay;
    }

    private void Update()
    {
        countDown -= Time.deltaTime;
        if(countDown <= 0f && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    private void Explode()
    {
        Debug.Log("Explode");
        Instantiate(explosiveForce, this.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
