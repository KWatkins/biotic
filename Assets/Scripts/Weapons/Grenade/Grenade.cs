using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Weapon
{
    [SerializeField]
    GameObject liveGrenadePrefab;
    protected override void CalculateShot()
    {
       GameObject newGreande = Instantiate(liveGrenadePrefab, transform.position + (fpsCam.transform.forward * .2f), Quaternion.identity);
        newGreande.GetComponent<Rigidbody>().AddForce(fpsCam.transform.forward * 20f, ForceMode.Impulse);
    }
}
