using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMG : Weapon
{
    protected override void CalculateShot()
    {
        if (Physics.Raycast(rayOrigin, CalculateBulletSpread() * Vector3.forward, out hit, gunStats.range, gunStats.layerMask))
        {
            OnRayCastHit();
        }
    }
}
