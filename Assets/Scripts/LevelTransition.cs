using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition : MonoBehaviour
{
    public Signal transitionLevelSignal;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            transitionLevelSignal.Raise();
            other.transform.parent = null;
            DontDestroyOnLoad(other.gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
