using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateTimer : MonoBehaviour
{
    [SerializeField] float timeToDelay;

    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(DelayDeactivate());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DelayDeactivate()
    {
        yield return new WaitForSeconds(timeToDelay);
        this.gameObject.SetActive(false);
    }
}
