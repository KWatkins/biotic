using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class Health : MonoBehaviour
{

    [SerializeField] public bool isPlayer = false;
    [SerializeField] private GameObject painIndicator;

    [SerializeField] public bool isEnemy = false;

    [SerializeField] LayerMask isGround;
    [SerializeField] Animator thisAnimator;

    [SerializeField] public float health = 50f;

    [SerializeField] bool spawnObjectOnDeath;
    [SerializeField] private GameObject spawnOnDeath;
    [SerializeField] bool spawnObjectBeforeDeath;
    [SerializeField] private GameObject spawnBeforeDeath;
    [SerializeField] bool setInactiveOnDeath;
    [SerializeField] private bool isDelayDeath;



    public bool isDead;
    private float timeRemaining = 0;

    public bool takingDamage = false;

    public enum DeathAnim { Default, LeftArm, Rightarm, Head, Torso, LowerBody }

    public DeathAnim deathAnim = DeathAnim.Default;


    private void OnEnable()
    {
        if (this.GetComponent<Animator>() == null)
        {
            if (this.GetComponent<Animator>() != null)
            {
                thisAnimator = this.GetComponent<Animator>();
            }
            else
            {
                if (this.GetComponentInChildren<Animator>() != null)
                {
                    thisAnimator = this.GetComponentInChildren<Animator>();
                }
            }
        }
    }

    private void Update()
    {

        if (health < 0)
        {
            health = 0;
            //isDead = true;
        }

        if (health <= 0 && !isDead && !isDelayDeath)
        {
            Die();
        }
    }
    public void TakeDamage(float amount)
    {
        if (!takingDamage && !isDead)
        {
            takingDamage = true;

            health -= amount;

            if (health <= 0f)
            {
                if (isDelayDeath)
                {
                    GameObject quickBurn = Instantiate(spawnBeforeDeath, gameObject.transform.position, Quaternion.identity);
                    quickBurn.transform.SetParent(this.gameObject.transform);
                    StartCoroutine(DelayDie());
                }
                else
                {
                    Debug.Log("I'm dead Jim!");
                    Die();
                }
            }
            else
            {
                Debug.Log("Just a scratch");
                if (isEnemy && !isDead)
                {
                    HitStagger(thisAnimator);
                }
            }

            if (isPlayer && painIndicator != null)
            {
                painIndicator.SetActive(true);
            }


            takingDamage = false;
        }

    }

    void HitStagger(Animator thisAnimator)
    {
        if (!isDead)
        {
            thisAnimator.SetTrigger("HitReaction");
            Debug.Log("HitReaction");
        }

    }

    public void Die()
    {
        int randomNumber = Random.Range(0, 2);

        if (!isDead)
        {

            if (spawnObjectOnDeath == true)
            {
                Instantiate(spawnOnDeath, gameObject.transform.position, Quaternion.identity);
            }

            if (isPlayer)
            {
                GameObject weaponHolder = GameObject.Find("WeaponHolder");
                if (weaponHolder != null)
                {
                    weaponHolder.SetActive(false);
                }
            }
            else if (isEnemy)
            {
                if (setInactiveOnDeath)
                {
                    gameObject.SetActive(false);
                }

                if (gameObject.GetComponentInChildren<NavMeshAgent>() != false)
                {
                    gameObject.GetComponentInChildren<NavMeshAgent>().enabled = false;
                }

                if (GetComponentInChildren<ThirdPersonCharacter>() != null)
                {
                    gameObject.GetComponentInChildren<ThirdPersonCharacter>().enabled = false;
                }


                if (randomNumber == 0)
                {
                    Debug.Log("Ragdoll");
                    GoRagdoll();
                }
                else if (randomNumber == 1 && !isDead)
                {
                    Debug.Log("Animate");
                    switch (deathAnim)
                    {
                        case DeathAnim.Default:
                            Debug.Log("Default");
                            thisAnimator.SetTrigger("Dying");
                            break;
                        case DeathAnim.Head:
                            Debug.Log("Head");
                            thisAnimator.SetTrigger("DeathHead");
                            break;
                        case DeathAnim.Torso:
                            Debug.Log("Torso");
                            thisAnimator.SetTrigger("DeathTorso");
                            break;
                        case DeathAnim.LowerBody:
                            Debug.Log("Lower");
                            thisAnimator.SetTrigger("DeathLowerBody");
                            break;
                        case DeathAnim.Rightarm:
                            Debug.Log("Rarm");
                            thisAnimator.SetTrigger("DeathArmRight");
                            break;
                        case DeathAnim.LeftArm:
                            Debug.Log("Larm");
                            thisAnimator.SetTrigger("DeathArmLeft");
                            break;
                    }
                }
            }
            else
            {
                if (this.gameObject.GetComponent<WorldItem>() != null)
                {
                    Debug.Log("World Item");
                    this.gameObject.GetComponent<WorldItem>().isNotActive = true;
                }
                Debug.Log("set inactive");
                gameObject.SetActive(false); //save load wont remember when something with unique ID is destroyed

            }
            isDead = true;
        }
    }

    public void GoRagdoll()
    {
        //Disables animator and disables kinematics to enable ragdolls
        Animator enemyAnim = this.gameObject.GetComponentInChildren<Animator>();
        enemyAnim.enabled = false;
        Rigidbody[] childrenRB = this.transform.GetComponentsInChildren<Rigidbody>();
        for (int i = 0; i < childrenRB.Length; i++)
        {
            childrenRB[i].isKinematic = false;
            childrenRB[i].collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }
    }

    public void damageOverTime()
    {

        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        else
        {
            TakeDamage(Random.Range(5, 15));
            timeRemaining = 1f;
        }
    }

    IEnumerator DelayDie()
    {
        Debug.Log("Delay Die");
        yield return new WaitForSeconds(1.5f);
        Die();
    }

}
