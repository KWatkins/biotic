using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveVertical : MonoBehaviour
{
    public void JumpEvent()
    {
        StartCoroutine(Lerp());
    }
    private IEnumerator Lerp()
    {
        Vector3 startposition = this.transform.position;
        Vector3 targetPosition = startposition + new Vector3(0, 50f, 0);
        Debug.Log("Started");
            transform.position = Vector3.Lerp(startposition, targetPosition, 1f * Time.deltaTime);

            yield return new WaitForEndOfFrame();
    }
}
